using UnityEngine;
using UnityEditor;

public class BuildPlayers {

	public static void PreExportStep() {
		//PlayerSettings.bundleVersion = "1.0.2";
		//PlayerSettings.Android.bundleVersionCode = 2;
		//PlayerSettings.iOS.buildNumber = "2";
		PlayerSettings.bundleVersion = ProjectConfig.APP_VERSION;
		PlayerSettings.Android.bundleVersionCode = ProjectConfig.APP_BUILDNUMBER;
		PlayerSettings.iOS.buildNumber = ProjectConfig.APP_BUILDNUMBER.ToString();
		Debug.Log("####################### END ##################");
	}

}

